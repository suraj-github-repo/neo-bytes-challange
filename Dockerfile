FROM php:7-fpm
ADD my_first_script.sh /
ENTRYPOINT ["/bin/bash", "/my_first_script.sh"]
